# CR-10 V2.5.2 Board Pinout

## Purpose

The purpose of this project is to attempt to document as fully as possible the availalbe pins on the creality V2.5.2 board used on the CR-10 v2 and v3.

The goal for the author personally was to;
1) Run a BLTouch 
2) Somehow, stop the always on fan when the hotend is not "hot" or otherwise in used
3) Use a second Extruder


## Results at this time

Refer to Creality-CR-10-pinout.pdf - pins identified as spare or non critical and generally available for use are highlighted in yellow.


### 1) BLTouch 
1) BLTouch pins stay intact though in the future D11 may be relocated to Y+ above the other BLTouch pins.


### 2) Always on fan

We have K-fan1 and K-fan2, which are joined, controlling the part cooling fan. There is no second mosfet driven PWM channel. The Heat2 (pin 7) PWM driven mosfet is available however the goal is to use this for a second extruder so its not considered for running a fan in this context.

The chosen solution at this time was to use pin D12 (and the accompaning 5V and GND) to drive an external relay. The extruder fan was cabling was rerun from the frame to the relay in order to achieve this.

In Marlin, Configuration_adv.h around like 480 needs to be modified like this:
`#define E0_AUTO_FAN_PIN 12`

### 3) Second Extruder

Still under consideration, however if the BLTouch pin at D11 is reloadted per 1) we have the whole grid at A9 avaialbe to drive an external stepper driver, maybe similar to https://core-electronics.com.au/tb6600-stepper-motor-driver.html.



## Software

Software is Marlin. See file cr-10-V2.5.2-Marlin-M43.



## Issues

There are current issues raised regarding missing information from the PDF files.



## References

https://www.arduino.cc/en/Hacking/PinMapping2560

https://3dprinting.stackexchange.com/questions/6535/controlling-more-fans-with-ramps-board 

https://github.com/MarlinFirmware/Marlin/issues/12961 

https://danieldoesmanythings.com/351/how-to-set-your-3d-printer-z-probe-offset/

https://www.pickysysadmin.ca/2020/08/16/marlin-2-x-for-a-cr-10-v3/

https://git.pickysysadmin.ca/FiZi/cr-10-v3-marlin-config 

https://www.th3dstudio.com/hc/downloads/unified-2-firmware/creality/creality-cr-10-v2-firmware-v2-5-2-board/

https://www.th3dstudio.com/hc/guides/bootloader/creality-v2-0-v2-1-v2-2-board-atmel-2560-icsp-programming-header-pinout/ 

https://www.th3dstudio.com/hc/product-information/3rd-party-control-boards/creality-boards/creality-v4-2-2-v4-2-7-board-bl-touch-wiring-options/

https://www.th3dstudio.com/hc/product-information/ezneo/ezneo-creality-v2-5-2-board-setup/ 

https://cdn.thingiverse.com/assets/67/42/7a/bf/ad/v3_Fixed_Creality_BLTOUCH_Guide.pdf 


### wiring
https://github.com/RudolphRiedel/CR-10_wiring/blob/master/CR-10_Wiring.pdf 

https://github.com/Creality3DPrinting/CR-10/tree/master/Circuit%20diagram/Motherboard 

